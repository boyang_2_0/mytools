@echo off

%1 mshta vbscript:CreateObject("Shell.Application").ShellExecute("cmd.exe","/c %~s0 ::","","runas",1)(window.close)&&exit

Title=PERSONAL更新程序
Color 02

taskkill /F /IM excel.exe
timeout /T 3

set path=%appData%\Microsoft\Excel\XLSTART\PERSONAL.XLSB

if exist %path% (
@del %path%
)

:: 下载文件
setlocal

:: 设置变量
set "url=https://gitee.com/boyang_2_0/mytools/raw/master/PERSONAL.XLSB"
set "destinationDir=%appData%\Microsoft\Excel\XLSTART"
set "fileName=PERSONAL.XLSB"  :: 如果URL中不包含文件名，你可以在这里指定

:: 确保目标目录存在
if not exist "%destinationDir%" (
    mkdir "%destinationDir%"
)

:: 构造完整的目标文件路径
set "targetFile=%destinationDir%\%fileName%"

:: 使用curl下载文件
curl -s -o "%targetFile%" "%url%"

:: 检查下载是否成功
if %errorlevel% neq 0 (
    echo 下载失败！
    exit /b %errorlevel%
) else (
    echo 更新已完成。%date%%time%
pause
)

endlocal
