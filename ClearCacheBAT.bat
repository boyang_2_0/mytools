@echo off
echo.
echo 正在安全地自动清除垃圾文件……
del /f /q /s %systemdrive%\*.tmp
del /f /q /s %systemdrive%\*._mp
del /f /q /s %systemdrive%\*.log
del /f /q /s %systemdrive%\*.gid
del /f /q /s %systemdrive%\*.chk
del /f /q /s %systemdrive%\*.old
del /f /q /s %windir%\*.bak
del /f /q /s %windir%\*.tmp
for /d %%i in (C:\Windows\LanguageOverlayCache\*) do (rd /s /q %%i)
for /d %%i in (C:\Windows\SystemTemp\*) do (rd /s /q %%i)
for /d %%i in (C:\Windows\Logs\*) do (rd /s /q %%i)
for /d %%i in (%windir%\prefetch\*) do (rd /s /q %%i)
for /d %%i in (%TEMP%\*) do (rd /s /q %%i)
for /d %%i in (%Systemroot%\Prefetch\*) do (rd /s /q %%i)
del /f /q /s "%USERPROFILE%\Cookies\*.*"
for /d %%i in (%USERPROFILE%\Recent\*) do (rd /s /q %%i)
del /f /q /s "%USERPROFILE%\Application Data\Microsoft\Office\Recent\*.*"
for /d %%i in (%USERPROFILE%\Local Settings\Temp\*) do (rd /s /q %%i)
for /d %%i in (%USERPROFILE%\AppData\LocalLow\Temp\*) do (rd /s /q %%i)
for /d %%i in (%ALLUSERSPROFILE%\Documents\DrWatson\*) do (rd /s /q %%i)
for /d %%i in (%USERPROFILE%\Local Settings\Temporary Internet Files\*) do (rd /s /q %%i)
del /f /q /s "%USERPROFILE%\AppData\Local\Temp"
for /d %%i in (%USERPROFILE%\AppData\local\KuashengV2\*) do (rd /s /q %%i)
REM del /f /q /s "%USERPROFILE%\AppData\Roaming"
for /d %%i in (C:\Windows\SoftwareDistribution\Download\*) do (rd /s /q %%i)
for /d %%i in (%USERPROFILE%\下载\*) do (rd /s /q %%i)
for /d %%i in (C:\$Recycle.bin\*) do (rd /s /q %%i)
for /d %%i in (D:\$Recycle.bin\*) do (rd /s /q %%i)
for /d %%i in (E:\$Recycle.bin\*) do (rd /s /q %%i)
for /d %%i in (F:\$Recycle.bin\*) do (rd /s /q %%i)
rd /s /q %windir%\temp & md %windir%\temp
if not exist %SystemRoot%\Minidump\NUL del /f /q /s /s %SystemRoot%\Minidump\*.*

echo 清除系统垃圾文件完成。
echo. & pause